const express = require('express'),
        app = express()
        request = require('request')
        mongoose = require('mongoose'),

mongoose.connect('mongodb://localhost/productsAPI');

app.get('/api/data', (req,res) => {
        var Product = require('./models/productModel'); //-- BookModel instance
        Product.find(req.query, (err,docs) => {
                if(err)
                    res.status(404).send("No products found");
                else
                    res.json(docs);
            });
})

app.listen(8081, () => console.log('backend service running🤓'))

